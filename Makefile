.PHONY: lab-kvm lab-virtualbox run clean

lab-kvm:
		cd vagrant/kvm && \
		vagrant up 
		echo "" > ~/.ssh/known_hosts
		ssh-copy-id -i ~/.ssh/id_ed25519.pub debian@192.168.56.20

lab-virtualbox:
		cd vagrant/virtualbox && \
		vagrant up
		echo "" > ~/.ssh/known_hosts
		ssh-copy-id -i ~/.ssh/id_ed25519.pub debian@192.168.56.20

run:
		./scripts/main.sh
		ansible-playbook -i inventory playbook/teleport.yml
		./scripts/clean.sh
clean:
		cd vagrant/kvm && \
		(vagrant destroy || true)
		cd vagrant/virtualbox && \
		(vagrant destroy || true)
			