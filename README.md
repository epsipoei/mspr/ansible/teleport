<div align="center">
<img src="docs/media/teleport-logo.png" width="300"/>
</div>


<br>
</br>

___

# Teleport

## **Summary**

[[*TOC*]]

### Installation automatisé de Teleport
___

- #### **Lab Virtualbox**
___

Sur virtualbox créer l'interface réseau

<img src="docs/media/network-virtualbox.png " width="600"/>

- #### **Installer Ansible**

    ```bash
    apt install ansible
    ```

- #### **Installer Vagrant**

    - https://developer.hashicorp.com/vagrant/install


    - Plugins
        - VirtualBox:
            ```bash
            vagrant plugin install vagrant-vbguest
            ```


- #### **Personnaliser le fichier vagrant**
___

- **sur virtualbox**
```bash
vagrant/virtualbox/vagrantfile
```
vous pourrez y definir l'ip, les ressources... ainsi que le path du script si vous souhaitez définir votre propre script.  
*par default l'ip utilisé pour le noeuds commence a 192.168.56.2# +i*

- #### **Personnaliser le fichier all.yml et hosts**
___

```bash
playbook/group_vars/all.yml
inventory/hosts
```
Lors du **"make run"** vous serez invité à définir les variables qui rempliront **automatiquement** le fichier **"all.yml"**.  
**Par contre le fichier hosts doit etre défini avant.**  
**ATTENTION !!** le nommage des hosts dois correspondre avec le nom des assets.

- #### **Install & Run with Make**
___

Personnaliser le fichier **Makefile** pour definir l'utilisateur et ip des serveur pour la partie ssh
```bash
ssh-copy-id -i ~/.ssh/id_ed25519.pub user@ip-srv
```

- **Install make**
    ```bash
    apt install make -y
    ```
- **Créer les vms**
    ```bash
    make lab-virtualbox
    ```
- **Executer Ansible**
    ```bash
    make run
    ```
- **Clean le Lab**
    ```bash
    make clean
    ```

- docs: <https://goteleport.com/docs/>

Lors du deploiement de l'agent sur les vm suivre les etapes sur l'ui

- curl en mode insecure ajouter -k  
*exemple:*  

    ```bash
    sudo bash -c "$(curl -k -fsSL <https://192.168.56.20/scripts/481128545407f7bde55e058b6a3df13d/install-node.sh>)"
    ```

- run teleport avec self-signed cert

    ```bash
    sudo teleport start -c /etc/teleport.yaml --insecure
    ```

Notes:

- Penser à éditer crontab pour executer la task au @reboot:
    ```bash
    @reboot sudo teleport start -c /etc/teleport.yaml --insecure
    ```